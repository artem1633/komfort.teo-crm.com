<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
?>
<div class="comment-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'created_at',
        ],
    ]) ?>

</div>
