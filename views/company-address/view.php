<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyAddress */
?>
<div class="company-address-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'address',
        ],
    ]) ?>

</div>
