<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Param */
?>
<div class="param-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
