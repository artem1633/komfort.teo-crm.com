<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Param */

?>
<div class="param-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
