<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressSpecification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-specification-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'specification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_date')->input('date') ?>

    <?= $form->field($model, 'start_date')->input('date') ?>

    <?= $form->field($model, 'reload_date')->input('date') ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'bill')->fileInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'start')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'specification_path')->fileInput(['maxlength' => true]) ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
