<?php
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\Param;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */

/**
 * @param int $type
 * @return array
 */
function getParamsByType($type){
    return \yii\helpers\ArrayHelper::map(Param::find()->where(['type' => $type])->all(), 'id', 'name');
}

$formatJs = <<< 'JS'
var formatRepo = function (color) {
    if (color.loading) {
        return color.text;
    }
    var markup = '<div class="row">' + 
    '<div class="col-sm-12">' +
        '<b style="margin-left:5px">' + color.code + '</b>' + 
        '<div style="display: inline-block; height: 10px; width: 30px; margin-left: 5px; background-color: '+color.rgb_hex+';"></div>'+
     '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (color) {
    return color.text || color.code;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

if($model->isNewRecord == false){
    $model->comments = \yii\helpers\ArrayHelper::getColumn(\app\models\ProductComment::find()->where(['product_id' => $model->id])->all(), 'comment_id');
} else {
    $model->count = 1;
    $model->price = 0;
}

?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <div id="door-designer-container" style="position: relative;">
                <img id="door-designer-img" src="/<?=$model->categoryParam != null ? $model->categoryParam->img_url : ''?>" alt="" style="width: 100%;height: 700px;object-fit: contain;position: absolute;top: 0;right: 0;">
                <img id="door-designer-img_handle" src="/<?=$model->handleParam != null ? $model->handleParam->img_url : ''?>" style="width: 100%;height: 700px;object-fit: contain;position: absolute;top: 0;right: 0;">
                <img id="door-designer-img_window_glass" src="/<?=$model->glassWindowParam != null ? $model->glassWindowParam->img_url : ''?>" style="width: 100%;height: 700px;object-fit: contain;position: absolute;top: 0;right: 0;">
                <img id="door-designer-img_chipper" src="/<?=$model->chipperParam != null ? $model->chipperParam->img_url : ''?>" style="width: 100%;height: 700px;object-fit: contain;position: absolute;top: 0;right: 0;">
                <img id="door-designer-img_transom" src="/<?=$model->transomParam != null ? $model->transomParam->img_url : ''?>" style="width: 100%;height: 700px;object-fit: contain;position: absolute;top: 0;right: 0;">
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'category')->dropDownList(getParamsByType(Param::TYPE_CATEGORY), ['prompt' => 'Выберите вариант']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'height')->input('number') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'width')->input('number') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'canvas_width')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'castle')->dropDownList(getParamsByType(Param::TYPE_CASTLE), ['prompt' => 'Выберите вариант']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'ral')->widget(\kartik\select2\Select2::className(), [
                        'initValueText' => $model->ral,
                        'data' => [1 => $model->ral],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'ajax' => [
                                'url' => \yii\helpers\Url::toRoute('colors', true),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ]
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'direction')->dropDownList(getParamsByType(Param::TYPE_DIRECTION), ['prompt' => 'Выберите вариант']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'metal')->dropDownList(getParamsByType(Param::TYPE_METAL), ['prompt' => 'Выберите вариант']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'handle')->dropDownList(getParamsByType(Param::TYPE_HANDLE), ['prompt' => 'Выберите вариант']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'transom')->dropDownList(getParamsByType(Param::TYPE_TRANSOM), ['prompt' => 'Выберите вариант']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'glass_window')->dropDownList(getParamsByType(Param::TYPE_GLASS_WINDOW), ['prompt' => 'Выберите вариант']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'chipper')->dropDownList(getParamsByType(Param::TYPE_CHIPPER), ['prompt' => 'Выберите вариант']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'closer')->dropDownList(getParamsByType(Param::TYPE_CLOSER), ['prompt' => 'Выберите вариант']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'delivery')->input('number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'install')->input('number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'count')->input('number', ['min' => '1']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'price')->input('number') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group  has-success">
                        <label class="control-label" for="product-price">Сумма</label>
                        <?php $sum = $model->price * $model->count; ?>
                        <input type="number" id="productSum" class="form-control"  value="<?=$sum?>" aria-invalid="false" disabled="disabled">
                        <div class="help-block"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'comments')->widget(\kartik\select2\Select2::className(), [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comment::find()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => 'Выберите примечание',
                            'multiple' => true
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'additional_information')->textarea(['rows' => 5]) ?>
                </div>
            </div>
        </div>
    </div>




    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>

<?php

if($model->category != null){
    $paramCategory = Param::find()->where(['id' => $model->category])->one();
    $paramCategoryPrice = $paramCategory->price;
    $paramCategorySizeCounting = json_encode($paramCategory->size_counting == 1 ? true : false);
} else {
    $paramCategoryPrice = 0;
    $paramCategorySizeCounting = json_encode(false);
}

if($model->direction != null){
    $paramDirection = Param::find()->where(['id' => $model->direction])->one();
    $paramDirectionPrice = $paramDirection->price;
    $paramDirectionSizeCounting = json_encode($paramDirection->size_counting == 1 ? true : false);
} else {
    $paramDirectionPrice = 0;
    $paramDirectionSizeCounting = json_encode(false);
}

if($model->metal != null){
    $paramMetal = Param::find()->where(['id' => $model->metal])->one();
    $paramMetalPrice = $paramMetal->price;
    $paramMetalSizeCounting = json_encode($paramMetal->size_counting == 1 ? true : false);
} else {
    $paramMetalPrice = 0;
    $paramMetalSizeCounting = json_encode(false);
}

if($model->castle != null){
    $paramCastle = Param::find()->where(['id' => $model->castle])->one();
    $paramCastlePrice = $paramCastle->price;
    $paramCastleSizeCounting = json_encode($paramCastle->size_counting == 1 ? true : false);
} else {
    $paramCastlePrice = 0;
    $paramCastleSizeCounting = json_encode(false);
}

if($model->transom != null){
    $paramTransom = Param::find()->where(['id' => $model->transom])->one();
    $paramTransomPrice = $paramTransom->price;
    $paramTransomSizeCounting = json_encode($paramTransom->size_counting == 1 ? true : false);
} else {
    $paramTransomPrice = 0;
    $paramTransomSizeCounting = json_encode(false);
}

if($model->glass_window != null){
    $paramGlassWindow = Param::find()->where(['id' => $model->glass_window])->one();
    $paramGlassWindowPrice = $paramGlassWindow->price;
    $paramGlassWindowSizeCounting = json_encode($paramGlassWindow->size_counting == 1 ? true : false);
} else {
    $paramGlassWindowPrice = 0;
    $paramGlassWindowSizeCounting = json_encode(false);
}

if($model->chipper != null){
    $paramChipper = Param::find()->where(['id' => $model->chipper])->one();
    $paramChipperPrice = $paramChipper->price;
    $paramChipperSizeCounting = json_encode($paramChipper->size_counting == 1 ? true : false);
} else {
    $paramChipperPrice = 0;
    $paramChipperSizeCounting = json_encode(false);
}

if($model->closer != null){
    $paramCloser = Param::find()->where(['id' => $model->closer])->one();
    $paramCloserPrice = $paramCloser->price;
    $paramCloserSizeCounting = json_encode($paramCloser->size_counting == 1 ? true : false);
} else {
    $paramCloserPrice = 0;
    $paramCloserSizeCounting = json_encode(false);
}

if($model->handle != null){
    $paramHandle = Param::find()->where(['id' => $model->handle])->one();
    $paramHandlePrice = $paramHandle->price;
    $paramHandleSizeCounting = json_encode($paramHandle->size_counting == 1 ? true : false);
} else {
    $paramHandlePrice = 0;
    $paramHandleSizeCounting = json_encode(false);
}

$height = $model->height != null ? $model->height : 0;
$width = $model->width != null ? $model->width : 0;
$delivery = $model->delivery != null ? $model->delivery : 0;
$install = $model->install != null ? $model->install : 0;
$size = $height * $width;

$count = $model->count != null ? $model->count : 1;

$script = "
var prices = {
    calcAttributes: {
        ".Param::TYPE_CATEGORY.": {
            price: $paramCategoryPrice,
            sizeCounting: $paramCategorySizeCounting,
        },
        ".Param::TYPE_DIRECTION.": {
            price: $paramDirectionPrice,
            sizeCounting: $paramDirectionSizeCounting,
        },
        canvasWidth: 0,
        ral: 0,
        ".Param::TYPE_METAL.": {
            price: $paramMetalPrice,
            sizeCounting: $paramMetalSizeCounting,
        },
        ".Param::TYPE_HANDLE.": {
            price: $paramHandlePrice,
            sizeCounting: $paramHandleSizeCounting,
        },
        ".Param::TYPE_CASTLE.": {
            price: $paramCastlePrice,
            sizeCounting: $paramCastleSizeCounting,
        },
        ".Param::TYPE_TRANSOM.": {
            price: $paramTransomPrice,
            sizeCounting: $paramTransomSizeCounting,
        },
        ".Param::TYPE_GLASS_WINDOW.": {
            price: $paramGlassWindowPrice,
            sizeCounting: $paramGlassWindowSizeCounting,
        },
        ".Param::TYPE_CHIPPER.": {
            price: $paramChipperPrice,
            sizeCounting: $paramChipperSizeCounting,
        },
        ".Param::TYPE_CLOSER.": {
            price: $paramCloserPrice,
            sizeCounting: $paramCloserSizeCounting,
        },
        delivery: $delivery,
        install: $install,
    },
    sizeAttributes: {
        height: $height,
        width: $width,
        additionalWidth: 0,
        size: $size,
    },
    count: $count,
    calcSize: function(){
        this.sizeAttributes.size = this.sizeAttributes.height * this.sizeAttributes.width;    
    },
    calculate: function(){
        this.calcSize();
        
        var sum = 0;
        for(var key in this.calcAttributes){
            var self = this.calcAttributes;
            if(typeof self[key] == 'number'){
                sum += self[key];
            } else if(typeof self[key] == 'object'){
                if(self[key].sizeCounting == true){
                    sum += self[key].price * this.sizeAttributes.size;
                } else {
                    sum += self[key].price;
                }
            }
        }
        
        var price = sum;
        sum = sum * this.count;
        
        $('#product-price').val(price);
        $('#productSum').val(sum);
    }
};

var DoorDesigner = function(){
};

DoorDesigner.setCategory = function(img){
    $('#door-designer-img').attr('src', '/'+img);
};

DoorDesigner.setHandle = function(img){
    $('#door-designer-img_handle').attr('src', '/'+img);
};

DoorDesigner.setWindowGlass = function(img){
    $('#door-designer-img_window_glass').attr('src', '/'+img);
};

DoorDesigner.setTransom = function(img){
    $('#door-designer-img_transom').attr('src', '/'+img);
};

DoorDesigner.setChipper = function(img){
    $('#door-designer-img_chipper').attr('src', '/'+img);
};

$('#product-width').change(function(){
    prices.sizeAttributes.width = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-height').change(function(){
    prices.sizeAttributes.height = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-delivery').change(function(){
    prices.calcAttributes.delivery = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-install').change(function(){
    prices.calcAttributes.install = parseFloat($(this).val() != '' ? $(this).val() : 0);
    prices.calculate();
});

$('#product-count').change(function(){
    prices.count = $(this).val();
    prices.calculate();
});

$('#product-price').change(function(){
    $('#productSum').val($('#product-price').val()*$('#product-count').val());
});

$('#product-category, #product-subcategory, #product-direction, #product-handle, #product-metal, #product-castle, #product-transom, #product-glass_window, #product-chipper, #product-closer').change(function(){
    var value = $(this).val();
    var ziro = false;
    
    if(value == ''){
        value = $(this).find('option:last-child').attr('value');
        ziro = true;
    }
    
    var id = $(this).attr('id');
    
    $.get('/param/view-ajax?id='+value, function(response){
        var price = response.price;
        var sizeCounting = response.size_counting == 1 ? true : false;
    
        if(id == 'product-category'){
            DoorDesigner.setCategory(response.img_url);
        }
        if(id == 'product-handle'){
            DoorDesigner.setHandle(response.img_url);
        }
        if(id == 'product-glass_window'){
            DoorDesigner.setWindowGlass(response.img_url);
        }
        if(id == 'product-chipper'){
            DoorDesigner.setChipper(response.img_url);
        }
        if(id == 'product-transom'){
            DoorDesigner.setTransom(response.img_url);
        }
        
        if(ziro == false){
            prices.calcAttributes[response.type] = {'price': price, 'sizeCounting': sizeCounting};
        } else {
            prices.calcAttributes[response.type] = {'price': 0, 'sizeCounting': sizeCounting};
        }
        prices.calculate();
    });
});


";

$this->registerJs($script, \yii\web\View::POS_READY);


?>
