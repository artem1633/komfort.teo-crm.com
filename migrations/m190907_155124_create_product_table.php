<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m190907_155124_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'user_id' => $this->integer()->comment('Менеджер'),
            'price' => $this->float()->comment('Цена'),
            'category' => $this->integer()->comment('Категория'),
            'subcategory' => $this->integer()->comment('Подкатегория'),
            'direction' => $this->integer()->comment('Направление'),
            'height' => $this->float()->comment('Высота'),
            'width' => $this->float()->comment('Ширина'),
            'additional_width' => $this->float()->comment('Дополнительная ширина'),
            'canvas_width' => $this->float()->comment('Ширина полотна'),
            'ral' => $this->string()->comment('RAL (Цвет)'),
            'metal' => $this->integer()->comment('Метал'),
            'castle' => $this->integer()->comment('Замок'),
            'handle' => $this->integer()->comment('Ручка'),
            'glass_window' => $this->integer()->comment('Стеклопакет'),
            'chipper' => $this->integer()->comment('Отбойник'),
            'closer' => $this->integer()->comment('Доводчик'),
            'transom' => $this->integer()->comment('Фрамуга'),
            'delivery' => $this->string()->comment('Доставка'),
            'install' => $this->string()->comment('Монтаж/Демонтаж'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-product-order_id',
            'product',
            'order_id'
        );

        $this->addForeignKey(
            'fk-product-order_id',
            'product',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-product-user_id',
            'product',
            'user_id'
        );

        $this->addForeignKey(
            'fk-product-user_id',
            'product',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-product-user_id',
            'product'
        );

        $this->dropIndex(
            'idx-product-user_id',
            'product'
        );

        $this->dropForeignKey(
            'fk-product-order_id',
            'product'
        );

        $this->dropIndex(
            'idx-product-order_id',
            'product'
        );

        $this->dropTable('product');
    }
}
