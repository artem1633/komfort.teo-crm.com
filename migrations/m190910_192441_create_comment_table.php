<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m190910_192441_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comment');
    }
}
