<?php

use yii\db\Migration;

/**
 * Handles adding additional_information to table `product`.
 */
class m190908_165323_add_additional_information_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'additional_information', $this->text()->after('install')->comment('Дополнительная информация'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'additional_information');
    }
}
