<?php

use yii\db\Migration;

/**
 * Class m190909_145759_add_foreign_key_to_product_table
 */
class m190909_145759_add_foreign_key_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
//        'order_id' => $this->integer()->comment('Заказ'),
//            'user_id' => $this->integer()->comment('Менеджер'),
//            'price' => $this->float()->comment('Цена'),
//            'category' => $this->integer()->comment('Категория'),
//            'direction' => $this->integer()->comment('Направление'),
//            'height' => $this->float()->comment('Высота'),
//            'width' => $this->float()->comment('Ширина'),
//            'additional_width' => $this->float()->comment('Дополнительная ширина'),
//            'canvas_width' => $this->float()->comment('Ширина полотна'),
//            'ral' => $this->string()->comment('RAL (Цвет)'),
//            'metal' => $this->integer()->comment('Метал'),
//            'castle' => $this->integer()->comment('Замок'),
//            'handle' => $this->integer()->comment('Ручка'),
//            'glass_window' => $this->integer()->comment('Стеклопакет'),
//            'chipper' => $this->integer()->comment('Отбойник'),
//            'closer' => $this->integer()->comment('Доводчик'),
//            'transom' => $this->integer()->comment('Фрамуга'),
//            'delivery' => $this->string()->comment('Доставка'),
//            'install' => $this->string()->comment('Монтаж/Демонтаж'),
//            'created_at' => $this->dateTime(),

        $this->addParamRelation([
            'category', 'direction', 'metal', 'castle',
            'handle', 'glass_window', 'chipper',
            'closer', 'transom'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->downParamRelation([
            'category', 'direction', 'metal', 'castle',
            'handle', 'glass_window', 'chipper',
            'closer', 'transom'
        ]);
    }

    private function addParamRelation($field)
    {
        if(is_array($field)){
            foreach ($field as $fld)
            {
                $this->createIndex(
                    'idx-product-'.$fld,
                    'product',
                    $fld
                );

                $this->addForeignKey(
                    'fk-product-'.$fld,
                    'product',
                    $fld,
                    'param',
                    'id',
                    'SET NULL'
                );
            }
        } else {
            $this->createIndex(
                'idx-product-'.$field,
                'product',
                $field
            );

            $this->addForeignKey(
                'fk-product-'.$field,
                'product',
                $field,
                'param',
                'id',
                'SET NULL'
            );
        }
    }

    private function downParamRelation($field)
    {
        if(is_array($field)){
            foreach ($field as $fld){
                $this->dropForeignKey(
                    'fk-product-'.$fld,
                    'product'
                );

                $this->dropIndex(
                    'idx-product-'.$fld,
                    'product'
                );
            }
        } else {
            $this->dropForeignKey(
                'fk-product-'.$field,
                'product'
            );

            $this->dropIndex(
                'idx-product-'.$field,
                'product'
            );
        }
    }
}
