<?php

use yii\db\Migration;

/**
 * Handles adding size_counting to table `param`.
 */
class m190908_150559_add_size_counting_column_to_param_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('param', 'size_counting', $this->boolean()->after('price')->defaultValue(0)->comment('Расчет по квадратуре'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('param', 'size_counting');
    }
}
