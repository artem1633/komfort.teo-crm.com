<?php

use yii\db\Migration;

/**
 * Handles dropping subcategory from table `product`.
 */
class m190908_165035_drop_subcategory_column_from_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('product', 'subcategory');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('product', 'subcategory', $this->integer()->after('category')->comment('Подкатегория'));
    }
}
