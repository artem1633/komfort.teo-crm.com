-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 12 2019 г., 15:50
-- Версия сервера: 5.7.28-0ubuntu0.16.04.2
-- Версия PHP: 7.2.25-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `komfort.teo-crm.com`
--

-- --------------------------------------------------------

--
-- Структура таблицы `address_specification`
--

CREATE TABLE `address_specification` (
  `id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL COMMENT 'Адрес',
  `specification` varchar(255) DEFAULT NULL COMMENT 'Спецификация',
  `specification_path` varchar(255) DEFAULT NULL COMMENT 'Спецификация Файл',
  `bill` varchar(255) DEFAULT NULL COMMENT 'Счет',
  `kp` varchar(255) DEFAULT NULL COMMENT 'КП',
  `start` varchar(255) DEFAULT NULL COMMENT 'Запуск',
  `payment_date` date DEFAULT NULL COMMENT 'Дата оплаты',
  `start_date` date DEFAULT NULL COMMENT 'Дата запуска',
  `reload_date` date DEFAULT NULL COMMENT 'Дата отгрузки'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `address_specification`
--

INSERT INTO `address_specification` (`id`, `address_id`, `specification`, `specification_path`, `bill`, `kp`, `start`, `payment_date`, `start_date`, `reload_date`) VALUES
(1, 1, '', '', 'uploads/Ui7mMA2XiE_Wb3VxJZkbOuE3tyYSFY1a.pdf', NULL, '', '2019-11-29', '2019-12-03', '2019-12-09');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `name`, `created_at`) VALUES
(1, 'Наличник стандарт', '2019-09-11 19:51:52'),
(2, 'Дополнительная петля', '2019-09-11 19:52:12');

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование',
  `email` varchar(255) DEFAULT NULL COMMENT 'Почта',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Телефон'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `company`
--

INSERT INTO `company` (`id`, `name`, `email`, `phone`) VALUES
(6, 'ООО "КапСтройЛидер"', 'snabprst@gmail.com', '8-958-812-79-77');

-- --------------------------------------------------------

--
-- Структура таблицы `company_address`
--

CREATE TABLE `company_address` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL COMMENT 'Компания',
  `address` varchar(500) DEFAULT NULL COMMENT 'Адрес'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `company_address`
--

INSERT INTO `company_address` (`id`, `company_id`, `address`) VALUES
(1, 6, '4-й Верхний Михайловский проезд 10к5');

-- --------------------------------------------------------

--
-- Структура таблицы `email_message`
--

CREATE TABLE `email_message` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1567879054),
('m170428_140722_create_user_table', 1567879057),
('m190907_152614_create_param_table', 1567879057),
('m190907_152811_create_order_table', 1567879057),
('m190907_155124_create_product_table', 1567879058),
('m190908_150559_add_size_counting_column_to_param_table', 1567965844),
('m190908_165035_drop_subcategory_column_from_product_table', 1567965844),
('m190908_165323_add_additional_information_column_to_product_table', 1567965844),
('m190908_165800_add_comment_column_to_product_table', 1567965845),
('m190908_170258_drop_parent_param_id_column_from_param_table', 1567965845),
('m190909_145759_add_foreign_key_to_product_table', 1568053191),
('m190910_182346_add_name_column_to_product_table', 1568185600),
('m190910_192441_create_comment_table', 1568185600),
('m190910_192607_create_junction_table_for_product_and_comment_tables', 1568185600),
('m190912_170222_add_count_column_to_product_table', 1568311281),
('m190912_172833_add_sum_column_to_product_table', 1568311281),
('m191031_130409_create_order_status_table', 1572528192),
('m191031_130524_add_status_id_to_order_table', 1572528192),
('m191126_144450_create_company_table', 1574790989),
('m191126_144550_create_company_address_table', 1574790989),
('m191126_144839_create_address_specification_table', 1574790989),
('m191126_153502_create_email_message_table', 1574790989),
('m191126_153720_add_role_column_to_user_table', 1574790989);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT 'Менеджер',
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование',
  `address` varchar(255) DEFAULT NULL COMMENT 'Адрес',
  `client` varchar(255) DEFAULT NULL COMMENT 'Клиент',
  `status_id` int(11) DEFAULT NULL COMMENT 'Статус',
  `date` date DEFAULT NULL COMMENT 'Дата',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `user_id`, `name`, `address`, `client`, `status_id`, `date`, `created_at`) VALUES
(11, 3, 'двери', '2-й Балтийский пер-к 4', 'АльтрумСтрой', NULL, NULL, '2019-09-18 09:13:28'),
(12, 3, 'Двери', 'Черняховского 17 к 1', 'АльтрумСтрой', NULL, NULL, '2019-09-18 09:24:42'),
(13, 3, 'Двери', 'Башиловская 26', 'АльтрумСтрой', NULL, NULL, '2019-09-18 09:43:39'),
(14, 3, 'Двери', '1-я Хуторская 5', 'АльтрумСтрой', NULL, NULL, '2019-09-18 09:52:46'),
(15, 3, 'Дверь ', '2-я Хуторская 22', 'АльтрумСтрой', NULL, NULL, '2019-09-18 10:05:06'),
(16, 3, 'Двери ', '1-я Амбулаторная к Б', 'АльтрумСтрой', NULL, NULL, '2019-09-18 10:13:54'),
(20, 3, 'Двери', 'Проспект Мира 76', 'Дефанс', NULL, NULL, '2019-09-18 11:29:38'),
(21, 3, 'Двери', 'Перекопская 22', 'Северстройлига', NULL, NULL, '2019-09-18 12:21:48'),
(22, 3, 'Двери ', 'Старый гай 4/1', 'Виктор ', NULL, NULL, '2019-09-19 11:32:23'),
(23, 3, 'Двери', 'Ивантеевская 10 ', 'Николай Григорьевич ', NULL, NULL, '2019-09-19 12:37:31'),
(24, 3, 'Двери', '5-я Парковая 39/1', '8 925 081 15 80', NULL, NULL, '2019-09-20 09:14:15'),
(25, 3, 'Двери ', 'Первомайская 76', 'Первомайская 76', NULL, NULL, '2019-09-20 10:09:07'),
(26, 3, 'Двери', 'Проспект мира 76', '8 905 555 10 12 Наталья ', NULL, NULL, '2019-09-20 11:18:51'),
(27, 2, 'Двери', '3-й Нижнелихоборский д.11', 'РутулСтрой', NULL, NULL, '2019-09-20 11:38:34'),
(28, 3, 'Двери', 'Проспект Мира 76', '8 905 555 10 12 Наталья', NULL, NULL, '2019-09-23 11:06:36'),
(29, 3, 'Люк', 'Донская 6 с 1', 'Наталья 8 905 555 10 12', NULL, NULL, '2019-09-24 10:49:08'),
(30, 3, 'Двери', 'Наталья ', '8 905 555 10 12', NULL, NULL, '2019-09-24 11:48:11'),
(31, 3, 'Двери', 'Крона 11к1', 'Лидер-строй ', NULL, NULL, '2019-09-24 13:47:28'),
(32, 3, 'Двери', 'Крона 15 к 1', 'Ледер-Строй', NULL, NULL, '2019-09-24 14:45:52'),
(127, 1, 'Тест', 'Теси', 'Клиент', NULL, NULL, '2019-12-12 15:25:53'),
(128, 1, 'Тест-2', 'Тест-2', 'Клиент-2', NULL, NULL, '2019-12-12 15:29:57'),
(129, 1, '11', '11', '', NULL, NULL, '2019-12-12 15:37:23');

-- --------------------------------------------------------

--
-- Структура таблицы `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `param`
--

CREATE TABLE `param` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование',
  `type` int(11) DEFAULT NULL COMMENT 'Тип',
  `price` float DEFAULT NULL COMMENT 'Цена',
  `size_counting` tinyint(1) DEFAULT '0' COMMENT 'Расчет по квадратуре',
  `img_url` varchar(255) DEFAULT NULL COMMENT 'Ссылка на картинку',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `param`
--

INSERT INTO `param` (`id`, `name`, `type`, `price`, `size_counting`, `img_url`, `created_at`) VALUES
(1, 'Входная', 0, 6500, 1, 'uploads/y-gC3ORco507_VntHrL8XC4IWr9TC6oR.png', '2019-09-07 18:00:05'),
(2, 'Техническая', 0, 10500, 0, 'uploads/idgcvFS4rQ8mphR0qcSrRRQUcyPsHZwQ.png', '2019-09-07 18:00:25'),
(3, 'Техническая (не стандарт)', 0, 6500, 1, 'uploads/wKvi5vCfxNxarfic7AEBzR7muK_fY9i1.png', '2019-09-07 18:00:49'),
(4, 'Техническая (двухстворчатая)', 0, 6500, 1, 'uploads/iOS90oB-tO_bXLAhO4O2Dpdq-iTC1x7f.png', '2019-09-07 18:01:03'),
(5, 'Наружу', 2, 0, 0, '', '2019-09-07 18:01:22'),
(6, 'Во внутрь', 2, 0, 0, '', '2019-09-07 18:01:35'),
(7, '1.5мм', 3, 0, 0, '', '2019-09-07 18:01:47'),
(8, 'Ключ/Ключ', 4, 0, 0, '', '2019-09-07 18:02:04'),
(9, 'Ключ/Барашек', 4, 500, 0, '', '2019-09-07 18:02:13'),
(10, 'Нету', 6, 0, 0, '', '2019-09-07 18:02:29'),
(11, '200х1500 Триплекс ', 6, 3000, 0, 'uploads/8iqxoHccxW-6IJj_44hL_rJJtBOjJohW.png', '2019-09-07 18:02:40'),
(12, '200х1200 Триплекс ', 6, 3000, 0, 'uploads/8iqxoHccxW-6IJj_44hL_rJJtBOjJohW.png', '2019-09-07 18:02:55'),
(13, 'Нету', 7, 0, 0, '', '2019-09-07 18:03:05'),
(14, 'Снизу', 7, 3000, 0, 'uploads/HNNvkgriQLpBx4CH81KXmrb6LT7HKrpw.png', '2019-09-07 18:03:17'),
(15, 'Нету', 8, 0, 0, '', '2019-09-07 18:03:27'),
(16, '1шт', 8, 1500, 0, '', '2019-09-07 18:03:49'),
(17, 'Нету', 9, 0, 0, '', '2019-09-07 18:04:00'),
(18, 'Глухая', 9, 0, 0, 'uploads/mOfqWAzRY-7VfuChneuoVZaPnFPEHQ9d.png', '2019-09-07 18:04:11'),
(19, 'стандарт', 7, 6000, 0, 'uploads/gZIL2B3daP1iBKtnSRp90WLosBGNKB42.png', '2019-09-08 09:48:05'),
(21, 'стеклопакет триплекс', 9, 4500, 0, 'uploads/7_Zav4ZUHARTJeCxN1TkVdYGJ3H1KvjT.png', '2019-09-08 10:03:12'),
(22, 'стеклопакет противопожарный', 9, 5000, 0, 'uploads/9UgJZ_W-IlwSlEovojm93nVVP7HoR1Ev.png', '2019-09-08 10:03:42'),
(24, '2шт', 8, 3000, 0, '', '2019-09-08 10:15:49'),
(25, 'нажимная', 5, 0, 0, 'uploads/LbN_xWEa_lLO4JlydTfN2kRu3jFtOukB.png', '2019-09-08 10:20:21'),
(26, 'скоба 1000мм (хром)', 5, 3000, 0, 'uploads/1gjNzfMq5Jeg2LvW7eJ-z0gf-DkU4pHl.png', '2019-09-08 10:21:18'),
(27, 'скоба 500мм (хром)', 5, 2500, 0, 'uploads/8U-Q5FbxiECqbZTyM_fgKWsJyfUYHb7Q.png', '2019-09-08 10:22:03'),
(28, 'скоба 300мм (хром)', 5, 2000, 0, 'uploads/bCmUBw5FcBTl5EpyV1B4yzPXIYoahziv.png', '2019-09-08 10:22:36'),
(29, '300х1200 Триплекс ', 6, 3000, 0, 'uploads/G6EWNB8Q7nOLo_zT3RZN0eGJ6EZ1uBf0.png', '2019-09-08 15:35:40'),
(30, '400х500 Триплекс ', 6, 3000, 0, 'uploads/s82AXelJhCPuTIoz6vCA38HdxU8Yqchb.png', '2019-09-08 15:36:10'),
(31, '300х500 Триплекс ', 6, 3000, 0, 'uploads/Y06JBHbH1GXf38ALTitWMMqsL6sw2VNh.png', '2019-09-08 15:36:32'),
(32, '400х800 Триплекс ', 6, 3000, 0, 'uploads/5ABZy0CcZlErbQr1qBgNQEjF9CmdPoT6.png', '2019-09-08 15:36:51'),
(33, '200х1500 Противопожарный', 6, 3500, 0, 'uploads/8iqxoHccxW-6IJj_44hL_rJJtBOjJohW.png', '2019-09-08 15:38:44'),
(34, '200х1200 Противопожарный', 6, 3500, 0, 'uploads/8iqxoHccxW-6IJj_44hL_rJJtBOjJohW.png', '2019-09-08 15:39:04'),
(35, '300х1200 Противопожарный', 6, 3500, 0, 'uploads/8iqxoHccxW-6IJj_44hL_rJJtBOjJohW.png', '2019-09-08 15:39:28'),
(36, '400х500 Противопожарный', 6, 3500, 0, 'uploads/I7kdMpeMI0TFw1yVbJ4IwJ0cuQ2JKYBa.png', '2019-09-08 15:39:59'),
(37, '300х500 Противопожарный', 6, 3500, 0, 'uploads/fxX1gkbhuRlIvAqGnnx6zYd7tSF2QRd5.png', '2019-09-08 15:40:39'),
(38, '400х800 Противопожарный', 6, 3500, 0, 'uploads/ckI-FjnXpe3GAXLNr6oteefQZyAGxX7h.png', '2019-09-08 15:41:01'),
(39, 'Скоба (Сварная)', 5, 0, 0, 'uploads/Q_ntD6q8dOhtxlbyjMWpQeLWeaEduWwY.png', '2019-09-08 15:44:16'),
(40, '1.2мм', 3, 0, 0, '', '2019-09-08 15:49:56'),
(41, '2мм', 3, 0, 0, '', '2019-09-08 15:50:20'),
(42, 'Противопожарная Ei 60', 0, 10500, 0, 'uploads/hk88B5mtJtn2TcfPx3M2IVjdbXeVEyDW.png', '2019-09-08 18:49:18'),
(43, 'Противопожарная (не стандарт)', 0, 6500, 1, 'uploads/5iOZiFZXM88NExYORyR9N5Kuo_t3mQMu.png', '2019-09-08 18:50:20'),
(44, 'Противопожарная Ei 60 (двухстворчатая) ', 0, 6500, 1, 'uploads/SwRRURIG7I4rWJ1mL1iOWkkkPXFT6Igu.png', '2019-09-08 18:51:21'),
(45, 'Люк противопожарный Ei 60 ', 0, 7500, 0, 'uploads/y_7Kx3WYb5okKCSwYkf6B1_MJzCE5KKr.png', '2019-09-08 18:54:30'),
(46, 'Люк противопожарный Ei 60  (не стандарт)', 0, 6500, 1, 'uploads/j4frdEDW0vLm_4RLKzGq8gxBYPs51KV6.png', '2019-09-08 18:54:54'),
(51, 'триплекс стандарт', 9, 4500, 0, 'uploads/UB8slWcEltCe09ZwZ7HNWW4vKG8BgVrg.png', '2019-09-12 10:01:58'),
(52, 'Противопожарная стандарт', 9, 5500, 0, 'uploads/rjdZMT22ls-_ONYXv6LK2vCjwtTY8N1R.png', '2019-09-12 10:02:33'),
(53, 'Глухая стандарт', 9, 0, 0, 'uploads/7XvQy_f2DPRII_YbkEZS-b5VbueWtb_N.png', '2019-09-12 10:04:29'),
(54, '150х1500 Триплекс', 6, 3000, 0, 'uploads/zzNv3c2Z00KMVIEAwgL9OsmhG_Ei4K0b.png', '2019-09-18 11:41:37'),
(55, '150х1500 Противопожарный', 6, 3500, 0, 'uploads/Xquoft4N8HmaAz36KJdJZEx9WsuaASeA.png', '2019-09-18 11:42:24'),
(56, '200х1600 Триплекс', 6, 3000, 0, 'uploads/cgseaP_5pki3uj92RoFhs4AyFN0U9kdR.png', '2019-09-18 12:38:41'),
(57, '200х1600 Противопожарный', 6, 3500, 0, 'uploads/UPeN0zxc1DdhaDdeWeIp4NaiY3e2qRbk.png', '2019-09-18 12:39:05'),
(60, '150х1200 Триплекс', 6, 3000, 0, 'uploads/3hiSB3AsO3jV2cvODL_W4shFFCJ4REem.png', '2019-09-26 16:02:39'),
(61, '150х1200 Противопожарный', 6, 3500, 0, 'uploads/Wf9qS4zBjG_snWIZeTFevXlPv-ibJ4Uq.png', '2019-09-26 16:03:27'),
(62, '800х1600 Противопожарный', 6, 3500, 0, 'uploads/eLxBdvaSuSTMkTQFkRspMDzvaj9dVvkT.png', '2019-10-11 12:41:42'),
(63, '100 X 1500 Триплекс ', 6, 3000, 0, 'uploads/fNru4uE4vRuTJe0hNn-17RqmSHt7GwlT.png', '2019-10-18 12:27:57'),
(64, '100 X 1500 Противопожарный ', 6, 3500, 0, 'uploads/VJQFhqU0KLZkIfNCu8kenufUMaIOvp_1.png', '2019-10-18 12:28:56'),
(65, '300 x 1300 Триплекс', 6, 3000, 0, 'uploads/t8kLpJ5xkSDrYimrYaSU6viomD5PDATm.png', '2019-10-21 13:19:02'),
(66, '300 x 1300 Противопожарный ', 6, 3500, 0, 'uploads/caYpgHZJMNz4fXWlwdgwF7qVMI_N12ox.png', '2019-10-21 13:19:56'),
(67, '300 x 800 Триплекс ', 6, 3000, 0, 'uploads/RZNBc5WmHHutVQLHwV2V6U0lt1bCJYz1.png', '2019-10-23 17:17:30'),
(68, '300 x 800 Противопожарный ', 6, 3500, 0, 'uploads/YhLZZoHDpI69GeHiBiXZ_vBNvMTDzRjz.png', '2019-10-23 17:18:17'),
(69, '300 x 700 Триплекс', 6, 3000, 0, 'uploads/fwJwVW8zALWq76E_-SlEgIBUOZYwe6pB.png', '2019-10-31 14:51:11'),
(70, '300 x 700 Противопожарный ', 6, 3500, 0, 'uploads/bLN7o3LxvTM0Z7swn4Ipas9qK1GuFtHA.png', '2019-10-31 14:52:03'),
(71, '300 x 1500 Триплекс ', 6, 3000, 0, 'uploads/--1eYUdRKoGhbqetBoUwQmbJQNhV793C.png', '2019-11-08 11:40:18'),
(72, '300 x  1500 Противопожарный ', 6, 3500, 0, 'uploads/aGCdGHPimJIXsugl-_Ql-SK_WxM71c5C.png', '2019-11-08 11:41:05'),
(73, '200 x 600 Триплекс', 6, 3000, 0, 'uploads/gXDtNDczCDpBy7zZcq5eTpTBk00w7B93.png', '2019-11-21 10:36:12'),
(74, '200 x 600 Противопожарный', 6, 3500, 0, 'uploads/-mDd-3L4HbXVBRZn62Rx07zmiFPbvxBW.png', '2019-11-21 10:36:49'),
(75, '300 x 400 Триплекс ', 6, 3000, 0, 'uploads/mJ2R7gTNB7qTkSm8W4wOz5ZmAjaxuZqn.png', '2019-11-22 13:33:53'),
(76, '300 x 400 Противопожарный ', 6, 3500, 0, 'uploads/7IKoN3w_OU_gDHbi8anZFhWdXscbi33D.png', '2019-11-22 13:34:30'),
(77, '500 x 1300 Триплекс ', 6, 3000, 0, 'uploads/9gG6zeNIvFaZDaVciJElQka0eb85o3N6.png', '2019-11-28 10:58:31'),
(78, '500 x 1300 Противопожарный ', 6, 3500, 0, 'uploads/KxDcJzgbDYi8In2ED-PRDB9aW5jS3XM7.png', '2019-11-28 10:59:12'),
(80, '300 x 1000 Треплекс', 6, 3000, 0, 'uploads/0xkB22ZE4LfsUFRMaE_6INvUS_drk53h.png', '2019-12-04 14:14:21'),
(81, '300 x 1000 Противопожарный ', 6, 3500, 0, 'uploads/iC58oSaK2nmgCLeFMvBBBtjngHuIZCzQ.png', '2019-12-04 14:15:00');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `order_id` int(11) DEFAULT NULL COMMENT 'Заказ',
  `user_id` int(11) DEFAULT NULL COMMENT 'Менеджер',
  `price` float DEFAULT NULL COMMENT 'Цена',
  `sum` float DEFAULT NULL COMMENT 'Сумма',
  `count` int(11) UNSIGNED DEFAULT NULL COMMENT 'Кол-во',
  `category` int(11) DEFAULT NULL COMMENT 'Категория',
  `direction` int(11) DEFAULT NULL COMMENT 'Направление',
  `height` float DEFAULT NULL COMMENT 'Высота',
  `width` float DEFAULT NULL COMMENT 'Ширина',
  `additional_width` float DEFAULT NULL COMMENT 'Дополнительная ширина',
  `canvas_width` float DEFAULT NULL COMMENT 'Ширина полотна',
  `ral` varchar(255) DEFAULT NULL COMMENT 'RAL (Цвет)',
  `metal` int(11) DEFAULT NULL COMMENT 'Метал',
  `castle` int(11) DEFAULT NULL COMMENT 'Замок',
  `handle` int(11) DEFAULT NULL COMMENT 'Ручка',
  `glass_window` int(11) DEFAULT NULL COMMENT 'Стеклопакет',
  `chipper` int(11) DEFAULT NULL COMMENT 'Отбойник',
  `closer` int(11) DEFAULT NULL COMMENT 'Доводчик',
  `transom` int(11) DEFAULT NULL COMMENT 'Фрамуга',
  `delivery` varchar(255) DEFAULT NULL COMMENT 'Доставка',
  `install` varchar(255) DEFAULT NULL COMMENT 'Монтаж/Демонтаж',
  `additional_information` text COMMENT 'Дополнительная информация',
  `comment` text COMMENT 'Примечание',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `order_id`, `user_id`, `price`, `sum`, `count`, `category`, `direction`, `height`, `width`, `additional_width`, `canvas_width`, `ral`, `metal`, `castle`, `handle`, `glass_window`, `chipper`, `closer`, `transom`, `delivery`, `install`, `additional_information`, `comment`, `created_at`) VALUES
(5, NULL, NULL, 1, 38175, NULL, NULL, 1, NULL, 2.1, 1.5, NULL, NULL, '1', 7, 9, 26, 12, 19, 16, 17, '700', '3000', '', '', '2019-09-10 10:10:47'),
(7, 'вход в подвал', NULL, 1, 24400, NULL, NULL, 2, NULL, 2, 1, NULL, NULL, '1', 7, NULL, NULL, 11, 19, 16, 18, '700', '2500', 'ЦУравоваяопаопао', NULL, '2019-09-11 19:04:28'),
(9, 'Подвал', NULL, 1, 18700, NULL, NULL, 2, 5, 2.1, 1.5, NULL, NULL, '1', 7, 9, 26, NULL, NULL, 16, 18, '700', '2500', 'Присьимтми', NULL, '2019-09-11 19:22:17'),
(11, 'Щитовая', NULL, 1, 20900, NULL, NULL, 42, 5, 2.1, 1.2, NULL, NULL, '7000', 7, 9, NULL, NULL, NULL, 16, 22, '700', '2500', 'Адалалслсл лвлалалал', NULL, '2019-09-11 19:33:40'),
(17, 'Вход', NULL, 1, 47550, NULL, 10, 1, 5, 2.6, 1.5, NULL, NULL, '1', 7, NULL, 26, 12, 19, 16, 21, '700', '3500', 'Монтажные работы 21.04.2300', NULL, '2019-09-12 18:24:21'),
(19, 'приквартирный холл', NULL, 1, 30450, NULL, 100, 43, 5, 2.5, 1, NULL, NULL, '7040', 7, 9, 25, 37, NULL, 16, 52, '700', '2500', 'Монтаж завтра', NULL, '2019-09-12 20:35:59'),
(42, 'мусорка', NULL, 1, 20700, NULL, 10, 42, 5, 2.8, 1.3, NULL, NULL, '7003', 7, 9, 25, NULL, NULL, NULL, 52, '700', '3500', 'палыан', NULL, '2019-09-16 18:52:27'),
(44, 'Дверь Входная', 11, 3, 37900, NULL, 3, 1, 5, 2.16, 1.47, NULL, NULL, '1', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', 'Количество 3 шт', NULL, '2019-09-18 09:17:11'),
(45, 'Дверь Входная', 12, 3, 37000, NULL, 3, 1, 5, 2.13, 1.43, NULL, NULL, '7040', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', 'Количество 3 шт ', NULL, '2019-09-18 09:36:11'),
(46, 'Дверь Входная', 13, 3, 35500, NULL, 4, 1, 5, 2.05, 1.36, NULL, NULL, '1', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', 'Количество 4 шт', NULL, '2019-09-18 09:49:51'),
(47, 'Дверь Входная', 14, 3, 37500, NULL, 3, 1, 5, 2.2, 1.4, NULL, NULL, '7040', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', 'Количество 3 шт', NULL, '2019-09-18 09:56:46'),
(48, 'Дверь Входная', 15, 3, 37500, NULL, 3, 1, 5, 2.22, 1.4, NULL, NULL, '7040', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', 'Количество 3 шт', NULL, '2019-09-18 10:10:48'),
(49, 'Дверь Входная', 16, 3, 40000, NULL, 3, 1, 5, 2.3, 1.5, NULL, NULL, '7040', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', 'Количество 3 шт ', NULL, '2019-09-18 10:17:04'),
(52, 'двери', 20, 3, 38589, NULL, 6, 1, 5, 2.1, 1.86, NULL, NULL, '1', 7, NULL, 39, 11, NULL, 16, 21, '700', '3500', 'Количество 6 шт \r\n(без вывоза мусора)', NULL, '2019-09-18 12:03:05'),
(53, 'Дверь Входная', 21, 3, 41700, NULL, 8, 1, 5, 2.26, 2.12, NULL, 600, '1', 7, NULL, 26, 10, 19, 16, NULL, '', '', '', NULL, '2019-09-18 12:47:17'),
(54, 'Мусорокамера', 21, 3, 10500, NULL, 8, 42, 5, 2.05, 0.9, NULL, 530, '7001', 7, 8, 25, NULL, NULL, NULL, NULL, '', '', '', NULL, '2019-09-18 12:54:28'),
(56, 'Запасной Выход', 21, 3, 22300, NULL, 1, 44, 5, 2.85, 1.2, NULL, 530, '7001', 7, 8, 25, NULL, NULL, NULL, 18, '', '', '', NULL, '2019-09-18 13:07:36'),
(57, 'Входная тамбурная в подъезд', 21, 3, 39900, NULL, 8, 4, 5, 2.33, 2.5, NULL, 530, '7001', 7, NULL, 28, NULL, NULL, 16, 18, '', '', 'Слева фрамуга,остекленная стеклопакетом 1200*800', NULL, '2019-09-18 13:22:10'),
(58, 'Типовой этаж', 21, 3, 14000, NULL, 136, 2, 5, 2.34, 0.95, NULL, 530, '7001', 7, 9, 25, 31, NULL, NULL, NULL, '', '', 'Доставка всех дверей 45000. Сроки изготовления 14 рабочих дней \r\n64 правые и 72 левые', NULL, '2019-09-18 13:30:54'),
(59, 'Двери ', 20, 3, NULL, NULL, 1, 1, 5, 2.1, 1.86, NULL, NULL, '1', 7, NULL, 39, 11, NULL, 16, 21, '', '', 'Остекление будет 150*1500 согласно вашему заказу\r\nРазмер фрамуги 1860*1000', NULL, '2019-09-19 09:19:36'),
(60, 'Тамбурная ', 22, 3, 29500, NULL, 1, 1, 5, 2.24, 1.46, NULL, NULL, '1', 7, NULL, 39, 32, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-19 11:39:49'),
(61, 'дверь ', 22, 3, 17700, NULL, 24, 42, 5, 2.1, 0.95, NULL, NULL, '1', 7, NULL, 25, 37, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-19 11:46:32'),
(62, 'на лестницу большая', 22, 3, 21850, NULL, 20, 43, 5, 2.1, 1, NULL, NULL, '1', 7, NULL, 39, 38, NULL, 16, NULL, '700', '2500', '', NULL, '2019-09-19 11:59:38'),
(63, 'балкон', 22, 3, 13700, NULL, 4, 42, 5, 2.2, 1, NULL, NULL, '1', 7, NULL, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-19 12:03:46'),
(64, 'люк', 22, 3, 9700, NULL, 12, 45, 5, 0.5, 0.6, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '1500', '', NULL, '2019-09-19 12:06:00'),
(65, 'Вход', 23, 3, 47900, NULL, 4, 1, 5, 3.2, 1.45, NULL, NULL, '1', 7, NULL, 26, 12, 19, 16, 18, '700', '3500', '', NULL, '2019-09-19 13:25:26'),
(66, 'Вход', 23, 3, 38500, NULL, 4, 1, 5, 2.3, 1.42, NULL, NULL, '1', 7, NULL, 26, 12, 19, 16, NULL, '700', '3000', '', NULL, '2019-09-19 13:28:33'),
(67, 'Тамбур ', 23, 3, 31500, NULL, 8, 4, 5, 2.3, 1.54, NULL, NULL, '1', 7, NULL, 39, 32, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-19 13:48:09'),
(68, 'Мусорокамера ', 23, 3, 14000, NULL, 8, 42, 5, 1.55, 0.94, NULL, NULL, '1', 7, NULL, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-19 13:51:08'),
(69, 'Подвал ', 23, 3, 14000, NULL, 1, 42, 5, 1.42, 0.94, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-19 14:02:40'),
(70, 'Подвал', 23, 3, 14000, NULL, 1, 42, 5, 1.6, 0.95, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-19 14:04:40'),
(71, 'Дверь крыша ', 23, 3, 14000, NULL, 16, 42, 5, 1.3, 1, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-19 14:07:11'),
(72, 'Чердак', 23, 3, 9700, NULL, 16, 45, 6, 0.9, 0.9, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '1500', '', NULL, '2019-09-19 14:12:15'),
(73, 'Вход', 24, 3, 40000, NULL, 4, 1, 5, 2.2, 1.4, NULL, NULL, '7040', 7, NULL, 26, 12, 14, 16, NULL, '700', '3000', '', NULL, '2019-09-20 09:19:39'),
(74, 'Тамбур', 24, 3, 34500, NULL, 4, 1, 5, 2.25, 1.55, NULL, NULL, '7040', 7, NULL, 39, 12, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 09:24:09'),
(75, 'Подвал', 24, 3, 15000, NULL, 1, 42, 5, 1.77, 0.93, NULL, NULL, '7040', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 09:36:23'),
(76, 'Подвал', 24, 3, 15000, NULL, 1, 42, 6, 1.83, 0.85, NULL, NULL, '1', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 09:42:36'),
(77, 'Подвал', 24, 3, 15000, NULL, 1, 42, 5, 1.94, 0.98, NULL, NULL, '1', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 09:44:11'),
(78, 'Подвал', 24, 3, 15000, NULL, 1, 42, 5, 1.87, 0.86, NULL, NULL, '7040', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 09:46:34'),
(79, 'Чердак', 24, 3, 10800, NULL, 4, 45, 5, 0.9, 0.9, NULL, NULL, '7040', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '1500', '', NULL, '2019-09-20 09:56:06'),
(80, 'Вход', 25, 3, 39500, NULL, 5, 1, 5, 2.25, 1.3, NULL, NULL, '1', 7, NULL, 39, 12, 14, 16, NULL, '700', '3000', '', NULL, '2019-09-20 10:13:45'),
(81, 'Двери', 25, 3, 39500, NULL, 1, 1, 5, 2.18, 0.87, NULL, NULL, '1', 7, NULL, 39, 12, 14, 16, NULL, '700', '3000', '', NULL, '2019-09-20 10:23:11'),
(82, 'Вход', 25, 3, 30500, NULL, 2, 1, 5, 2.27, 1.47, NULL, NULL, '8002', 7, NULL, 39, 10, NULL, 16, NULL, '700', '3000', 'Щеколда внутри.', NULL, '2019-09-20 10:27:42'),
(83, 'Подвал', 25, 3, 15000, NULL, 1, 42, 5, 1.5, 1, NULL, NULL, '8002', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 10:39:02'),
(84, 'Подвал', 25, 3, 15000, NULL, 1, 42, 5, 2.18, 0.88, NULL, NULL, '8002', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 10:41:05'),
(85, 'Подвал', 25, 3, 15000, NULL, 3, 42, 5, 2.05, 0.95, NULL, NULL, '8002', 40, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 10:47:34'),
(86, 'Вход', 26, 3, 23500, NULL, 1, 1, 5, 2.5, 0.93, NULL, NULL, '7040', 7, NULL, 39, 54, 13, 16, NULL, '700', '3000', '', NULL, '2019-09-20 11:37:19'),
(87, 'Вход', 26, 3, 23500, NULL, 2, 1, 5, 2.45, 0.95, NULL, NULL, '7040', 7, NULL, 39, 54, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 11:39:05'),
(88, 'тамбур', 27, 2, 25400, NULL, 1, 4, 5, 1.24, 2.13, NULL, NULL, '1', 7, NULL, 39, 29, NULL, 16, 17, '700', '3000', '', NULL, '2019-09-20 11:40:44'),
(89, 'тамбур', 27, 2, 27600, NULL, 1, 4, 5, 2.17, 1.37, NULL, NULL, '1', 7, NULL, 39, 29, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 11:43:09'),
(90, 'Тамбур', 27, 2, 28000, NULL, 1, 4, 5, 2.17, 1.4, NULL, NULL, '1', 7, NULL, 39, 29, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 11:44:23'),
(91, 'Подвал', 27, 2, 17100, NULL, 1, 43, 5, 2.18, 0.94, NULL, NULL, '1', 7, 9, 25, NULL, NULL, NULL, 53, '700', '2500', '', NULL, '2019-09-20 11:46:40'),
(92, 'Вход', 26, 3, 38925, NULL, 1, 1, 5, 3, 1.55, NULL, NULL, '7040', 7, NULL, 39, 54, NULL, 16, NULL, '700', '3500', '', NULL, '2019-09-20 11:47:45'),
(93, 'Подвал', 27, 2, 13700, NULL, 1, 42, 5, 2.05, 0.8, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 11:47:55'),
(94, 'Подвал', 27, 2, 14700, NULL, 1, 43, 5, 2.08, 0.85, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 11:49:43'),
(95, 'Вход', 26, 3, 31300, NULL, 1, 1, 5, 2.35, 1.5, NULL, NULL, '7040', 7, NULL, 39, 54, 13, 16, NULL, '700', '3000', '', NULL, '2019-09-20 11:52:05'),
(96, 'Тамбур', 26, 3, 19500, NULL, 1, 42, 5, 2.65, 0.95, NULL, NULL, '7040', 7, NULL, 39, 38, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 11:58:23'),
(97, 'Тамбур', 26, 3, 19500, NULL, 2, 42, 5, 2.6, 1, NULL, NULL, '7040', 7, NULL, 39, 38, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 12:05:31'),
(98, 'Тамбур', 26, 3, 19500, NULL, 1, 42, 5, 2.55, 1.5, NULL, NULL, '7040', 7, NULL, 39, 38, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-20 12:13:45'),
(99, 'Тамбур проходная', 26, 3, 43500, NULL, 3, 44, 5, 3.1, 1.7, NULL, NULL, '7040', 7, NULL, 39, 38, NULL, 16, 18, '700', '3500', '', NULL, '2019-09-20 12:20:28'),
(100, 'Вход проходная', 26, 3, 42500, NULL, 3, 44, 5, 3.1, 1.65, NULL, NULL, '7040', 7, NULL, 39, 38, NULL, 16, 18, '700', '3500', '', NULL, '2019-09-20 12:22:41'),
(101, 'Подвал', 26, 3, 14500, NULL, 1, 42, 5, 2.5, 0.93, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:24:32'),
(102, 'Подвал', 26, 3, 22500, NULL, 1, 44, 5, 2.2, 1.3, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:26:06'),
(103, 'Подвал', 26, 3, 14500, NULL, 2, 42, 5, 2.43, 0.94, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:28:18'),
(104, 'Подвал', 26, 3, 14500, NULL, 1, 42, 5, 2.3, 1.35, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:29:30'),
(105, 'Мусорокамера', 26, 3, 14500, NULL, 1, 42, 5, 2.42, 0.95, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:32:04'),
(106, 'Мусорокамера', 26, 3, 14500, NULL, 1, 42, 5, 2.5, 0.85, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:33:54'),
(107, 'Мусорокамера ', 26, 3, 14500, NULL, 1, 42, 5, 2.45, 0.83, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-20 12:37:26'),
(108, 'Мусорокамера', 26, 3, 14000, NULL, 1, 42, 5, 2.06, 0.85, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:41:36'),
(109, 'Мусорокамера', 26, 3, 14000, NULL, 1, 42, 5, 1.92, 0.8, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:43:28'),
(110, 'Щитовая', 26, 3, 14000, NULL, 1, 42, 5, 1.75, 0.8, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:47:11'),
(111, 'Щитовая', 26, 3, 13000, NULL, 1, 42, 5, 1.1, 0.83, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '1800', '', NULL, '2019-09-20 12:50:31'),
(112, 'Дверь чердак', 26, 3, 14000, NULL, 2, 42, 5, 1.9, 0.85, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:52:06'),
(113, 'Дверь чердак', 26, 3, 14000, NULL, 1, 42, 5, 1.75, 0.86, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:53:21'),
(114, 'Дверь чердак ', 26, 3, 14000, NULL, 1, 42, 5, 1.8, 0.85, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:56:29'),
(115, 'Дверь чердак', 26, 3, 14000, NULL, 1, 42, 5, 1.89, 0.9, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-20 12:58:38'),
(116, 'Вход', 28, 3, 25500, NULL, 1, 1, 5, 2.5, 0.93, NULL, NULL, '8002', 7, NULL, 28, 54, NULL, 16, 18, '700', '3000', '', NULL, '2019-09-23 11:51:51'),
(117, 'Вход', 28, 3, 25500, NULL, 2, 1, 5, 2.45, 0.95, NULL, NULL, '1', 7, NULL, 28, 54, NULL, 16, 18, '700', '3000', '', NULL, '2019-09-23 11:55:22'),
(118, 'Вход', 28, 3, 40925, NULL, 1, 1, 5, 3, 1.55, NULL, NULL, '8002', 7, NULL, 28, 54, NULL, 16, 18, '700', '3500', '', NULL, '2019-09-23 11:58:22'),
(119, 'Вход', 28, 3, 33300, NULL, 1, 1, 5, 2.35, 1.5, NULL, NULL, '8002', 7, NULL, 28, 54, NULL, 16, 18, '700', '3000', '', NULL, '2019-09-23 12:00:55'),
(120, 'Подвал', 28, 3, 18900, NULL, 1, 43, 5, 2.5, 0.93, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, 15, NULL, '700', '3000', '', NULL, '2019-09-23 12:53:49'),
(121, 'Подвал', 28, 3, 22300, NULL, 1, 43, 5, 2.2, 1.3, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-23 13:07:43'),
(122, 'Подвал', 28, 3, 18600, NULL, 2, 43, 5, 2.43, 0.94, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-23 13:10:53'),
(123, 'Подвал', 28, 3, 23900, NULL, 1, 43, 5, 2.3, 1.35, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-23 13:11:51'),
(124, 'Со стороны проспекта ', 28, 3, 45500, NULL, 3, 44, 5, 3.1, 1.65, NULL, NULL, '7034', 7, 8, 25, 37, NULL, 16, 18, '700', '3500', 'Двери будут согласно тех заданию.\r\nВо второй створке предусмотрен стеклопакет  ', NULL, '2019-09-23 13:31:41'),
(125, 'Со стороны двора', 28, 3, 43500, NULL, 3, 44, 5, 3.1, 1.7, NULL, NULL, '7034', 7, 8, 25, 34, NULL, 16, 18, '700', '3500', '', NULL, '2019-09-23 13:40:54'),
(126, 'Тамбур', 28, 3, 25100, NULL, 1, 43, 5, 2.65, 0.95, NULL, NULL, '7034', 7, NULL, 39, 34, NULL, 16, 53, '700', '3000', '', NULL, '2019-09-23 13:44:38'),
(127, 'Тамбур', 28, 3, 26100, NULL, 2, 43, 5, 2.6, 1, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, 53, '700', '3500', '', NULL, '2019-09-23 13:52:06'),
(128, 'Тамбур', 28, 3, 34100, NULL, 1, 43, 5, 2.55, 1.5, NULL, NULL, '7034', 7, NULL, 39, 34, NULL, 16, 53, '700', '3500', '', NULL, '2019-09-23 14:02:41'),
(129, 'Донская 6 с 1', 29, 3, 17500, NULL, 3, 43, 5, 2.12, 1.02, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 11:45:44'),
(130, 'Вход', 30, 3, 38000, NULL, 6, 1, 5, 2.89, 1.37, NULL, NULL, '7040', 7, NULL, 28, NULL, 13, 16, 21, '700', '3500', '', NULL, '2019-09-24 11:57:37'),
(131, 'Щитовая ', 30, 3, 14500, NULL, 1, 42, 5, 2.1, 0.95, NULL, NULL, '7040', 7, 9, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 12:02:14'),
(132, 'Донская 6 с 2', 29, 3, 17500, NULL, 4, 43, 5, 2.09, 1.05, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 12:18:41'),
(133, 'Анатолия Живова 3', 29, 3, 14000, NULL, 8, 42, 5, 1.99, 0.96, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 12:32:31'),
(134, 'Верхняя Масловка 24', 29, 3, 16000, NULL, 4, 43, 5, 2.12, 0.92, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 12:35:07'),
(135, 'Верхняя Масловка 8', 29, 3, 10000, NULL, 4, 45, 5, 0.99, 0.95, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '1500', '', NULL, '2019-09-24 12:37:18'),
(136, 'Красина 24/28', 29, 3, 14000, NULL, 3, 42, 5, 1.9, 0.92, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 12:40:06'),
(137, 'Ломоносовский проспект 4с1', 29, 3, 18300, NULL, 8, 43, 5, 2.16, 1.02, NULL, NULL, '7040', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-24 12:43:29'),
(138, 'Вход в подъезд', 31, 3, 23250, NULL, 4, 44, 5, 2.15, 1.5, NULL, NULL, '1', 7, NULL, 39, NULL, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-24 13:56:29'),
(139, 'Тамбур', 31, 3, 26000, NULL, 4, 44, 5, 2.2, 1.14, NULL, NULL, '1', 7, NULL, 39, 35, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-24 14:02:15'),
(140, 'Мусорокамера', 31, 3, 11500, NULL, 3, 42, 5, 1.8, 0.88, NULL, NULL, '1', 7, 8, 39, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 14:04:45'),
(141, 'Подвал', 31, 3, 11400, NULL, 4, 42, 5, 1.85, 0.8, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 14:06:51'),
(142, 'Люк', 31, 3, 8400, NULL, 4, 45, 5, 0.9, 0.9, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '1500', '', NULL, '2019-09-24 14:08:47'),
(143, 'Вход', 32, 3, 23250, NULL, 4, 44, 5, 2.15, 1.5, NULL, NULL, '1', 7, NULL, 39, NULL, NULL, 16, NULL, '700', '3000', '', NULL, '2019-09-24 14:50:24'),
(144, 'Тамбур', 32, 3, 26000, NULL, 4, 43, 5, 2.2, 1.41, NULL, NULL, '1', 7, NULL, 39, 35, NULL, NULL, NULL, '700', '3000', '', NULL, '2019-09-24 14:55:43'),
(145, 'Мусорокамера', 32, 3, 11500, NULL, 3, 42, 5, 1.85, 0.88, NULL, NULL, '1', 7, 8, 39, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 14:58:07'),
(146, 'Подвал', 32, 3, 11400, NULL, 2, 42, 5, 1.85, 0.8, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '2500', '', NULL, '2019-09-24 15:00:26'),
(147, 'Люк', 32, 3, 8400, NULL, 4, 45, 5, 0.9, 0.9, NULL, NULL, '1', 7, 8, 25, NULL, NULL, NULL, NULL, '700', '1500', '', NULL, '2019-09-24 15:01:55'),
(304, 'Тамбур', 11, 3, 28200, NULL, 1, 43, 5, 2.22, 1.35, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 17:59:33'),
(305, 'Тамбур', 11, 3, 24100, NULL, 1, 43, 5, 2.21, 1.07, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:00:43'),
(306, 'Тамбур', 11, 3, 23900, NULL, 1, 43, 5, 2.2, 1.06, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:02:04'),
(307, 'Тамбур', 16, 3, 31200, NULL, 1, 44, 5, 2.3, 1.5, NULL, NULL, '1', 7, NULL, 39, 35, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:05:16'),
(308, 'Тамбур', 16, 3, 31400, NULL, 1, 44, 5, 2.25, 1.55, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:06:47'),
(309, 'Тамбур', 16, 3, 31400, NULL, 1, 44, 5, 2.25, 1.55, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:07:56'),
(310, 'Тамбур', 12, 3, 29000, NULL, 1, 44, 5, 2.15, 1.45, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:10:32'),
(311, 'Тамбур ', 12, 3, 21200, NULL, 1, 43, 5, 2.13, 0.9, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:11:46'),
(312, 'Тамбур', 12, 3, 40600, NULL, 1, 44, 5, 2.13, 2.3, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:13:46'),
(313, 'Тамбур', 13, 3, 26400, NULL, 1, 44, 5, 2.16, 1.26, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:18:13'),
(314, 'Тамбур', 13, 3, 26300, NULL, 1, 44, 5, 2.08, 1.3, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:20:08'),
(315, 'Тамбур', 13, 3, 25300, NULL, 1, 44, 5, 2.04, 1.25, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:21:15'),
(316, 'Тамбур', 13, 3, 25400, NULL, 1, 44, 5, 2.14, 1.2, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:23:13'),
(317, 'Тамбур ', 14, 3, 28200, NULL, 1, 44, 5, 2.18, 1.37, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:28:42'),
(318, 'Тамбур', 14, 3, 28300, NULL, 1, 44, 5, 2.18, 1.38, NULL, NULL, '1', 7, NULL, 39, 34, NULL, 16, NULL, '700', '3000', '', NULL, '2019-10-18 18:29:49'),
(626, 'Дверь', 127, 1, 17000, NULL, 1, 2, 5, 1900, 1000, NULL, 900, '2007', 7, 8, NULL, 10, 13, 16, 17, '2500', '2500', '', NULL, '2019-12-12 15:28:51'),
(627, 'Дверь-2', 128, 1, 17500, NULL, 1, 2, 5, 1500, 1000, NULL, 950, '1001', 7, 8, 27, 10, 14, 16, 17, '', '', '', NULL, '2019-12-12 15:31:27'),
(628, 'Дверь ещё одна', 128, 1, 33500, NULL, 1, 2, 5, 2100, 1500, NULL, 900, '5007', 7, 8, 25, 33, 19, 24, 52, '2500', '2500', '', NULL, '2019-12-12 15:48:54');

-- --------------------------------------------------------

--
-- Структура таблицы `product_comment`
--

CREATE TABLE `product_comment` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_comment`
--

INSERT INTO `product_comment` (`id`, `product_id`, `comment_id`) VALUES
(11, 17, 1),
(12, 17, 2),
(14, 19, 1),
(15, 19, 2),
(89, 42, 1),
(90, 42, 2),
(103, 54, 1),
(104, 56, 1),
(161, 626, 1),
(162, 627, 1),
(164, 628, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL COMMENT 'Логин',
  `role` int(11) DEFAULT NULL COMMENT 'Роль',
  `name` varchar(255) DEFAULT NULL COMMENT 'ФИО',
  `password_hash` varchar(255) NOT NULL COMMENT 'Зашифрованный пароль',
  `is_deletable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Можно удалить или нельзя'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `role`, `name`, `password_hash`, `is_deletable`) VALUES
(1, 'admin', 0, 'Админ', '$2y$13$pRJO.jy8oIplWhDXyM7JBuwAyNu1ouWhkuN7XnPq8lADZxVSGRLcS', 0),
(2, 'Comfort', 0, ' Руденко Сергей', '$2y$13$.9wBeCPQVj5f7nzSARkp6erqFnqfBIE1v4I54grqnRJJnXvdiZIVS', 1),
(3, 'serega', 0, 'Кочетов Сергей', '$2y$13$YyBbg.MhMlHp3eZ35.N8LuMUkfW3D3nnrFzSw5O5fYkdkL3SkIb9i', 1),
(5, 'olenka.nefedova.93@mail.ru', 1, 'Нефедова Ольга Валерьевна', '$2y$13$S.WRuFjru4xDV34AnRbUXO4BqGN1DyLKZZTimu9rXNyYMGqQJZnoe', 1),
(6, '1@1.ru', 1, '1@1.ru', '$2y$13$eKLwAGbzNaY4JmjAMeX4mejpjIam1ipV7k5BYaXf7sYwXgje13tLC', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `address_specification`
--
ALTER TABLE `address_specification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-address_specification-address_id` (`address_id`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `company_address`
--
ALTER TABLE `company_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-company_address-company_id` (`company_id`);

--
-- Индексы таблицы `email_message`
--
ALTER TABLE `email_message`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-order-user_id` (`user_id`),
  ADD KEY `idx-order-status_id` (`status_id`);

--
-- Индексы таблицы `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `param`
--
ALTER TABLE `param`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-product-order_id` (`order_id`),
  ADD KEY `idx-product-user_id` (`user_id`),
  ADD KEY `idx-product-category` (`category`),
  ADD KEY `idx-product-direction` (`direction`),
  ADD KEY `idx-product-metal` (`metal`),
  ADD KEY `idx-product-castle` (`castle`),
  ADD KEY `idx-product-handle` (`handle`),
  ADD KEY `idx-product-glass_window` (`glass_window`),
  ADD KEY `idx-product-chipper` (`chipper`),
  ADD KEY `idx-product-closer` (`closer`),
  ADD KEY `idx-product-transom` (`transom`);

--
-- Индексы таблицы `product_comment`
--
ALTER TABLE `product_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-product_comment-product_id` (`product_id`),
  ADD KEY `idx-product_comment-comment_id` (`comment_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `address_specification`
--
ALTER TABLE `address_specification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `company_address`
--
ALTER TABLE `company_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `email_message`
--
ALTER TABLE `email_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT для таблицы `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `param`
--
ALTER TABLE `param`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=629;
--
-- AUTO_INCREMENT для таблицы `product_comment`
--
ALTER TABLE `product_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `address_specification`
--
ALTER TABLE `address_specification`
  ADD CONSTRAINT `fk-address_specification-address_id` FOREIGN KEY (`address_id`) REFERENCES `company_address` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `company_address`
--
ALTER TABLE `company_address`
  ADD CONSTRAINT `fk-company_address-company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk-order-status_id` FOREIGN KEY (`status_id`) REFERENCES `order_status` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-order-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk-product-castle` FOREIGN KEY (`castle`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-category` FOREIGN KEY (`category`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-chipper` FOREIGN KEY (`chipper`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-closer` FOREIGN KEY (`closer`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-direction` FOREIGN KEY (`direction`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-glass_window` FOREIGN KEY (`glass_window`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-handle` FOREIGN KEY (`handle`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-metal` FOREIGN KEY (`metal`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-order_id` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-product-transom` FOREIGN KEY (`transom`) REFERENCES `param` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk-product-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_comment`
--
ALTER TABLE `product_comment`
  ADD CONSTRAINT `fk-product_comment-comment_id` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-product_comment-product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
