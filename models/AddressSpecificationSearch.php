<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AddressSpecification;

/**
 * AddressSpecificationSearch represents the model behind the search form about `app\models\AddressSpecification`.
 */
class AddressSpecificationSearch extends AddressSpecification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'address_id'], 'integer'],
            [['specification', 'specification_path', 'bill', 'kp', 'start', 'payment_date', 'start_date', 'reload_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddressSpecification::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'address_id' => $this->address_id,
            'payment_date' => $this->payment_date,
            'start_date' => $this->start_date,
            'reload_date' => $this->reload_date,
        ]);

        $query->andFilterWhere(['like', 'specification', $this->specification])
            ->andFilterWhere(['like', 'specification_path', $this->specification_path])
            ->andFilterWhere(['like', 'bill', $this->bill])
            ->andFilterWhere(['like', 'kp', $this->kp])
            ->andFilterWhere(['like', 'start', $this->start]);

        return $dataProvider;
    }
}
