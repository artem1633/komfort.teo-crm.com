<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['id', 'order_id', 'user_id', 'category', 'direction', 'metal', 'castle', 'handle', 'glass_window', 'chipper', 'closer', 'transom'], 'integer'],
            [['height', 'width', 'additional_width', 'canvas_width', 'delivery', 'install', 'price'], 'number'],
            [['ral', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'price' => $this->price,
            'category' => $this->category,
            'direction' => $this->direction,
            'height' => $this->height,
            'width' => $this->width,
            'additional_width' => $this->additional_width,
            'canvas_width' => $this->canvas_width,
            'metal' => $this->metal,
            'castle' => $this->castle,
            'handle' => $this->handle,
            'glass_window' => $this->glass_window,
            'chipper' => $this->chipper,
            'closer' => $this->closer,
            'transom' => $this->transom,
            'delivery' => $this->delivery,
            'install' => $this->install,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ral', $this->ral]);

        return $dataProvider;
    }
}
