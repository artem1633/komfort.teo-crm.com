<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "param".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $type Тип
 * @property boolean $size_counting Расчет по квадратуре
 * @property double $price Цена
 * @property string $img_url Ссылка на картинку
 * @property string $created_at
 *
 */
class Param extends \yii\db\ActiveRecord
{
    const TYPE_CATEGORY = 0;
//    const TYPE_SUBCATEGORY = 1;
    const TYPE_DIRECTION = 2;
    const TYPE_METAL = 3;
    const TYPE_CASTLE = 4;
    const TYPE_HANDLE = 5;
    const TYPE_GLASS_WINDOW = 6;
    const TYPE_CHIPPER = 7;
    const TYPE_CLOSER = 8;
    const TYPE_TRANSOM = 9;

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'param';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type'], 'integer'],
            [['price'], 'number'],
            ['file', 'file', 'skipOnEmpty' => true],
            [['size_counting', 'created_at'], 'safe'],
            [['name', 'img_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип',
            'price' => 'Цена',
            'size_counting' => 'Расчет по квадратуре',
            'img_url' => 'Ссылка на картинку',
            'file' => 'Изображение',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->file != null){
            $fileName = Yii::$app->security->generateRandomString();
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }
            $path = "uploads/{$fileName}.{$this->file->extension}";
            $this->file->saveAs($path);
            if($this->img_url != null && file_exists($this->img_url)){
                unlink($this->img_url);
            }
            $this->img_url = $path;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_CATEGORY => 'Категория',
//            self::TYPE_SUBCATEGORY => 'Подкатегория',
            self::TYPE_DIRECTION => 'Направление',
            self::TYPE_METAL => 'Метал',
            self::TYPE_CASTLE => 'Замок',
            self::TYPE_HANDLE => 'Ручка',
            self::TYPE_GLASS_WINDOW => 'Стеклопакет',
            self::TYPE_CHIPPER => 'Отбойник',
            self::TYPE_CLOSER => 'Доводчик',
            self::TYPE_TRANSOM => 'Фрамуга'
        ];
    }
}
