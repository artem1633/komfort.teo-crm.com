<?php

namespace app\models;

use app\validators\ParamTypeValidator;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name Название
 * @property int $order_id Заказ
 * @property int $user_id Менеджер
 * @property double $price Цена
 * @property double $sum Сумма
 * @property integer $count Кол-во
 * @property int $category Категория
 * @property int $direction Направление
 * @property double $height Высота
 * @property double $width Ширина
 * @property double $additional_width Дополнительная ширина
 * @property double $canvas_width Ширина полотна
 * @property string $ral RAL (Цвет)
 * @property int $metal Метал
 * @property int $castle Замок
 * @property int $handle Ручка
 * @property int $glass_window Стеклопакет
 * @property int $chipper Отбойник
 * @property int $closer Доводчик
 * @property int $transom Фрамуга
 * @property string $delivery Доставка
 * @property string $install Монтаж/Демонтаж
 * @property string $additional_information Дополнительная информация
 * @property string $comment Примечание
 * @property string $created_at
 *
 * @property string $imgPath Изображение
 *
 * @property Param $castleParam
 * @property Param $categoryParam
 * @property Param $chipperParam
 * @property Param $closerParam
 * @property Param $directionParam
 * @property Param $glassWindowParam
 * @property Param $handleParam
 * @property Param $metalParam
 * @property Order $order
 * @property Param $transomParam
 * @property User $user
 * @property ProductComment[] $productComments
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $comments;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category'], 'required'],
            [['order_id', 'user_id', 'count', 'category', 'direction', 'metal', 'castle', 'handle', 'glass_window', 'chipper', 'closer', 'transom'], 'integer'],
            ['count', 'default', 'value' => 0],
            [['height', 'width', 'additional_width', 'canvas_width', 'price'], 'number'],
            [['created_at', 'comments'], 'safe'],
            [['name', 'ral', 'delivery', 'install'], 'string', 'max' => 255],
            [['additional_information', 'comment'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['castle'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['castle' => 'id']],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['category' => 'id']],
            [['chipper'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['chipper' => 'id']],
            [['closer'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['closer' => 'id']],
            [['direction'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['direction' => 'id']],
            [['glass_window'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['glass_window' => 'id']],
            [['handle'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['handle' => 'id']],
            [['metal'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['metal' => 'id']],
            [['transom'], 'exist', 'skipOnError' => true, 'targetClass' => Param::className(), 'targetAttribute' => ['transom' => 'id']],

            // type validation
            ['category', ParamTypeValidator::className(), 'type' => Param::TYPE_CATEGORY],
//            ['subcategory', ParamTypeValidator::className(), 'type' => Param::TYPE_SUBCATEGORY],
            ['direction', ParamTypeValidator::className(), 'type' => Param::TYPE_DIRECTION],
            ['metal', ParamTypeValidator::className(), 'type' => Param::TYPE_METAL],
            ['castle', ParamTypeValidator::className(), 'type' => Param::TYPE_CASTLE],
            ['handle', ParamTypeValidator::className(), 'type' => Param::TYPE_HANDLE],
            ['glass_window', ParamTypeValidator::className(), 'type' => Param::TYPE_GLASS_WINDOW],
            ['chipper', ParamTypeValidator::className(), 'type' => Param::TYPE_CHIPPER],
            ['closer', ParamTypeValidator::className(), 'type' => Param::TYPE_CLOSER],
            ['transom', ParamTypeValidator::className(), 'type' => Param::TYPE_TRANSOM],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'order_id' => 'Заказ',
            'count' => 'Кол-во',
            'user_id' => 'Менеджер',
            'price' => 'Цена',
            'category' => 'Категория',
            'direction' => 'Направление',
            'height' => 'Высота',
            'width' => 'Ширина',
            'additional_width' => 'Дополнительная ширина',
            'canvas_width' => 'Ширина полотна',
            'ral' => 'RAL (Цвет)',
            'metal' => 'Метал',
            'castle' => 'Замок',
            'handle' => 'Ручка',
            'glass_window' => 'Стеклопакет',
            'chipper' => 'Отбойник',
            'closer' => 'Доводчик',
            'transom' => 'Фрамуга',
            'delivery' => 'Доставка',
            'install' => 'Монтаж/Демонтаж',
            'additional_information' => 'Дополнительная информация',
            'created_at' => 'Дата и время создания',
            'comments' => 'Примечание'
        ];
    }


    /**
     * @inheritdoc
     */
    public function getSum()
    {
        return $this->price * $this->count;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->comments != null){
            ProductComment::deleteAll(['product_id' => $this->id]);
            foreach ($this->comments as $comment){
                (new ProductComment(['product_id' => $this->id, 'comment_id' => $comment]))->save(false);
            }
        }

        if($this->categoryParam != null && $this->categoryParam->img_url != ''){
            $img = imagecreatefrompng($this->categoryParam->img_url);
        }
        if($this->handleParam != null && $this->handleParam->img_url != ''){
            $img2 = imagecreatefrompng($this->handleParam->img_url);
            imagecopy($img, $img2, 0, 0, 0, 0, 1100, 1100);
        }
        if($this->glassWindowParam != null && $this->glassWindowParam->img_url != ''){
            $img3 = imagecreatefrompng($this->glassWindowParam->img_url);
            imagecopy($img, $img3, 0, 0, 0, 0, 1000, 1000);
        }
        if($this->transomParam != null && $this->transomParam->img_url != ''){
            $img4 = imagecreatefrompng($this->transomParam->img_url);
            imagecopy($img, $img4, 0, 0, 0, 0, 1000, 1000);
        }
        if($this->chipperParam != null && $this->chipperParam->img_url != ''){
            $img5 = imagecreatefrompng($this->chipperParam->img_url);
            imagecopy($img, $img5, 0, 0, 0, 0, 1500, 1500);
        }

        if(is_dir('product') == false){
            mkdir('product');
        }

        imagepng($img, 'product/image'.$this->id.'.png');
    }

    /**
     * @inheritdoc
     */
    public function getImgPath()
    {
       return "product/image{$this->id}.png";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCastleParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'castle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChipperParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'chipper']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCloserParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'closer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectionParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'direction']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGlassWindowParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'glass_window']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHandleParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'handle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransomParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'transom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetalParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'metal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductComments()
    {
        return $this->hasMany(ProductComment::className(), ['product_id' => 'id']);
    }
}
